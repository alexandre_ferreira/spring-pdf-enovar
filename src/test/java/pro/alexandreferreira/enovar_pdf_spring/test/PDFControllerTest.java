package pro.alexandreferreira.enovar_pdf_spring.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import pro.alexandreferreira.enovar_pdf_spring.controllers.PDFController;
import pro.alexandreferreira.enovar_pdf_spring.models.PDF;
import pro.alexandreferreira.enovar_pdf_spring.utils.HashUtils;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(value = PDFController.class, secure = false)
public class PDFControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private HashUtils hashUtils;

    private ObjectMapper mapper = new ObjectMapper();

    @Test
    public void testPdfHash() throws Exception {

        byte[] samplePdfBytes = IOUtils.toByteArray(getClass().getClassLoader()
                .getResourceAsStream("sample.pdf"));

        MockMultipartFile pdfFileMultipart = new MockMultipartFile("sample.pdf", "",
                "application/pdf", samplePdfBytes);

        String cpf = "116.982.250-91";

        mockMvc.perform(MockMvcRequestBuilders.multipart("/pdf")
                .file("pdfFile", pdfFileMultipart.getBytes())
                .param("cpf", cpf)
                .characterEncoding("UTF-8"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("hash").exists())
                .andDo(result -> {
                    String response = result.getResponse().getContentAsString();

                    PDF pdf = mapper.readValue(response, PDF.class);

                    String expectedBaseHash = String.format("%s|%s|%d", cpf, "127.0.0.1",
                            pdf.getCreatedDate().getTime());

                    Assert.assertEquals(expectedBaseHash, hashUtils.decrypt(pdf.getHash()));

                });
    }

    @Test
    public void testInvalidCPF() throws Exception {

        byte[] samplePdfBytes = IOUtils.toByteArray(getClass().getClassLoader()
                .getResourceAsStream("sample.pdf"));

        MockMultipartFile pdfFileMultipart = new MockMultipartFile("sample.pdf", "",
                "application/pdf", samplePdfBytes);

        String cpf = "";

        mockMvc.perform(MockMvcRequestBuilders.multipart("/pdf")
                .file("pdfFile", pdfFileMultipart.getBytes())
                .param("cpf", cpf)
                .characterEncoding("UTF-8"))
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.error[0]").value("CPF deve ser no padrao: 000.000.000-00"));
    }



}
