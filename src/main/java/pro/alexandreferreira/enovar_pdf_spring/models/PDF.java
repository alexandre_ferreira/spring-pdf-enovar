package pro.alexandreferreira.enovar_pdf_spring.models;

import java.util.Date;

public class PDF {

    private long id;
    private String cpf;
    private Date createdDate;
    private Long time;
    private String sourceIp;
    private String hash;

    public PDF() {
    }

    public PDF(long id, String cpf, Date createdDate, Long time, String sourceIp, String hash) {
        this.id = id;
        this.cpf = cpf;
        this.createdDate = createdDate;
        this.time = time;
        this.sourceIp = sourceIp;
        this.hash = hash;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getSourceIp() {
        return sourceIp;
    }

    public void setSourceIp(String sourceIp) {
        this.sourceIp = sourceIp;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }
}
