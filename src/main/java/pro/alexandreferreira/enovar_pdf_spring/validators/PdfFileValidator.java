package pro.alexandreferreira.enovar_pdf_spring.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

@Component
public class PdfFileValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return MultipartFile.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        MultipartFile file  = (MultipartFile) target;

        if (!file.getContentType().equals("application/pdf")) {
            errors.rejectValue("pdfFile", "pdfFile.not_valid_pdf");
        }
    }
}
