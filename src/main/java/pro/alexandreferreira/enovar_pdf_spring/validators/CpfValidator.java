package pro.alexandreferreira.enovar_pdf_spring.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class CpfValidator implements ConstraintValidator<CPF, String> {

    private Pattern pattern;

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return pattern.matcher(value).find();
    }

    @Override
    public void initialize(CPF constraintAnnotation) {
        pattern = Pattern.compile("(^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$)");
    }
}
