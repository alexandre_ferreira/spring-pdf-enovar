package pro.alexandreferreira.enovar_pdf_spring;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import javax.annotation.PostConstruct;
import javax.servlet.annotation.MultipartConfig;
import java.security.Security;
import java.util.TimeZone;

@SpringBootApplication
@MultipartConfig(fileSizeThreshold = 20 * 1024 * 1024) // Upload max 20 MB
@ComponentScan({"pro.alexandreferreira.enovar_pdf_spring"})
public class Application {

    @PostConstruct
    void started() {
        TimeZone.setDefault(TimeZone.getTimeZone("America/Fortaleza"));
    }

    public static void main(String[] args) {
        Security.setProperty("crypto.policy", "unlimited");
        SpringApplication.run(Application.class, args);
    }

}
