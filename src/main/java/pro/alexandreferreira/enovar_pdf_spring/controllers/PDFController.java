package pro.alexandreferreira.enovar_pdf_spring.controllers;

import com.itextpdf.text.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pro.alexandreferreira.enovar_pdf_spring.models.PDF;
import pro.alexandreferreira.enovar_pdf_spring.utils.HashUtils;
import pro.alexandreferreira.enovar_pdf_spring.utils.PDFUtils;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;


@RestController
@Validated
public class PDFController {

    private final HashUtils hashUtils;
    private final PDFUtils pdfUtils;

    private final AtomicLong counterId = new AtomicLong();

    @Autowired()
    public PDFController(HashUtils hashUtils, PDFUtils pdfUtils) {
        this.hashUtils = hashUtils;
        this.pdfUtils = pdfUtils;
    }


    @RequestMapping(value = "/pdf", method = RequestMethod.POST)
    public PDF createPDFHash(@Valid @RequestParam(value = "pdfFile") MultipartFile pdfFileMultipart,
                             @Valid @RequestParam("cpf")
                             @Pattern(regexp = "(^\\d{3}\\x2E\\d{3}\\x2E\\d{3}\\x2D\\d{2}$)",
                                     message = "CPF deve ser no padrao: 000.000.000-00")
                                     String cpf,
                             HttpServletRequest request) {

        Date currentDate = Calendar.getInstance().getTime();


        String remoteAddress = request.getRemoteAddr();
        String baseHash = String.format("%s|%s|%d", cpf, remoteAddress, currentDate.getTime());

        String hash = hashUtils.encrypt(baseHash);
        String pdfFilePath = System.getProperty("user.dir") + File.separator + "temp_pdfs";
        String pdfName = String.format("%s.pdf", hash);
        try {
            pdfUtils.addFooter(pdfFileMultipart.getInputStream(), pdfFilePath, pdfName, hash);
        } catch (IOException | DocumentException e) {
            throw new RuntimeException(e);
        }

        return new PDF(counterId.incrementAndGet(), cpf, currentDate, currentDate.getTime(),
                request.getRemoteAddr(), hash);
    }
}
