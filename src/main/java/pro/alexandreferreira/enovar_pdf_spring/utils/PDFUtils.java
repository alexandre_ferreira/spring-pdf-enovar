package pro.alexandreferreira.enovar_pdf_spring.utils;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@Component
public class PDFUtils {

    public void addFooter(InputStream inputStream, String path, String fileName, String hash)
            throws IOException, DocumentException {

        PdfReader reader = new PdfReader(inputStream);

        File directory = new File(path);
        if (! directory.exists()){
            boolean created = directory.mkdir();
            if (!created) {
                throw new RuntimeException("Não foi possível criar a pasta: " + path);
            }

        }
        String filePath = directory.getName() + File.separator + fileName;
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(filePath));

        PdfContentByte pagecontent;
        for (int i = 1; i <= reader.getNumberOfPages(); i++) {
            pagecontent = stamper.getUnderContent(i);
            Font ffont = new Font(Font.FontFamily.UNDEFINED, 8, Font.ITALIC);
            ColumnText.showTextAligned(pagecontent, Element.ALIGN_RIGHT,
                    new Phrase(hash, ffont), 559, 50, 0);
        }

        stamper.setFormFlattening(true);
        stamper.close();
        reader.close();
    }
}
