package pro.alexandreferreira.enovar_pdf_spring.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.stereotype.Component;

@Component
public class HashUtils {

    @Value("${security.hash.salt}")
    private String hashSalt;

    @Value("${security.hash.password}")
    private String hashPassword;

    public String encrypt(String stringToEncrypt ) {
        return Encryptors.text(hashPassword, hashSalt).encrypt(stringToEncrypt);
    }

    public String decrypt(String encryptedString) {
        return Encryptors.text(hashPassword, hashSalt).decrypt(encryptedString);
    }

}
